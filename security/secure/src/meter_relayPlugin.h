

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from meter_relay.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef meter_relayPlugin_1882284457_h
#define meter_relayPlugin_1882284457_h

#include "meter_relay.h"

struct RTICdrStream;

#ifndef pres_typePlugin_h
#include "pres/pres_typePlugin.h"
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

#define two_MeterPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
#define two_MeterPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
#define two_MeterPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

#define two_MeterPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
#define two_MeterPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

/* --------------------------------------------------------------------------------------
Support functions:
* -------------------------------------------------------------------------------------- */

NDDSUSERDllExport extern two_Meter*
two_MeterPluginSupport_create_data_w_params(
    const struct DDS_TypeAllocationParams_t * alloc_params);

NDDSUSERDllExport extern two_Meter*
two_MeterPluginSupport_create_data_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern two_Meter*
two_MeterPluginSupport_create_data(void);

NDDSUSERDllExport extern RTIBool 
two_MeterPluginSupport_copy_data(
    two_Meter *out,
    const two_Meter *in);

NDDSUSERDllExport extern void 
two_MeterPluginSupport_destroy_data_w_params(
    two_Meter *sample,
    const struct DDS_TypeDeallocationParams_t * dealloc_params);

NDDSUSERDllExport extern void 
two_MeterPluginSupport_destroy_data_ex(
    two_Meter *sample,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
two_MeterPluginSupport_destroy_data(
    two_Meter *sample);

NDDSUSERDllExport extern void 
two_MeterPluginSupport_print_data(
    const two_Meter *sample,
    const char *desc,
    unsigned int indent);

/* ----------------------------------------------------------------------------
Callback functions:
* ---------------------------------------------------------------------------- */

NDDSUSERDllExport extern PRESTypePluginParticipantData 
two_MeterPlugin_on_participant_attached(
    void *registration_data, 
    const struct PRESTypePluginParticipantInfo *participant_info,
    RTIBool top_level_registration, 
    void *container_plugin_context,
    RTICdrTypeCode *typeCode);

NDDSUSERDllExport extern void 
two_MeterPlugin_on_participant_detached(
    PRESTypePluginParticipantData participant_data);

NDDSUSERDllExport extern PRESTypePluginEndpointData 
two_MeterPlugin_on_endpoint_attached(
    PRESTypePluginParticipantData participant_data,
    const struct PRESTypePluginEndpointInfo *endpoint_info,
    RTIBool top_level_registration, 
    void *container_plugin_context);

NDDSUSERDllExport extern void 
two_MeterPlugin_on_endpoint_detached(
    PRESTypePluginEndpointData endpoint_data);

NDDSUSERDllExport extern void    
two_MeterPlugin_return_sample(
    PRESTypePluginEndpointData endpoint_data,
    two_Meter *sample,
    void *handle);    

NDDSUSERDllExport extern RTIBool 
two_MeterPlugin_copy_sample(
    PRESTypePluginEndpointData endpoint_data,
    two_Meter *out,
    const two_Meter *in);

/* ----------------------------------------------------------------------------
(De)Serialize functions:
* ------------------------------------------------------------------------- */

NDDSUSERDllExport extern RTIBool 
two_MeterPlugin_serialize(
    PRESTypePluginEndpointData endpoint_data,
    const two_Meter *sample,
    struct RTICdrStream *stream, 
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
two_MeterPlugin_deserialize_sample(
    PRESTypePluginEndpointData endpoint_data,
    two_Meter *sample, 
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
two_MeterPlugin_serialize_to_cdr_buffer(
    char * buffer,
    unsigned int * length,
    const two_Meter *sample); 

NDDSUSERDllExport extern RTIBool 
two_MeterPlugin_deserialize(
    PRESTypePluginEndpointData endpoint_data,
    two_Meter **sample, 
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
two_MeterPlugin_deserialize_from_cdr_buffer(
    two_Meter *sample,
    const char * buffer,
    unsigned int length);    
NDDSUSERDllExport extern DDS_ReturnCode_t
two_MeterPlugin_data_to_string(
    const two_Meter *sample,
    char *str,
    DDS_UnsignedLong *str_size, 
    const struct DDS_PrintFormatProperty *property);    

NDDSUSERDllExport extern RTIBool
two_MeterPlugin_skip(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    RTIBool skip_encapsulation,  
    RTIBool skip_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern unsigned int 
two_MeterPlugin_get_serialized_sample_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);    

NDDSUSERDllExport extern unsigned int 
two_MeterPlugin_get_serialized_sample_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
two_MeterPlugin_get_serialized_sample_min_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int
two_MeterPlugin_get_serialized_sample_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment,
    const two_Meter * sample);

/* --------------------------------------------------------------------------------------
Key Management functions:
* -------------------------------------------------------------------------------------- */
NDDSUSERDllExport extern PRESTypePluginKeyKind 
two_MeterPlugin_get_key_kind(void);

NDDSUSERDllExport extern unsigned int 
two_MeterPlugin_get_serialized_key_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
two_MeterPlugin_get_serialized_key_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern RTIBool 
two_MeterPlugin_serialize_key(
    PRESTypePluginEndpointData endpoint_data,
    const two_Meter *sample,
    struct RTICdrStream *stream,
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
two_MeterPlugin_deserialize_key_sample(
    PRESTypePluginEndpointData endpoint_data,
    two_Meter * sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
two_MeterPlugin_deserialize_key(
    PRESTypePluginEndpointData endpoint_data,
    two_Meter ** sample,
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
two_MeterPlugin_serialized_sample_to_key(
    PRESTypePluginEndpointData endpoint_data,
    two_Meter *sample,
    struct RTICdrStream *stream, 
    RTIBool deserialize_encapsulation,  
    RTIBool deserialize_key, 
    void *endpoint_plugin_qos);

/* Plugin Functions */
NDDSUSERDllExport extern struct PRESTypePlugin*
two_MeterPlugin_new(void);

NDDSUSERDllExport extern void
two_MeterPlugin_delete(struct PRESTypePlugin *);

#define two_RelayPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
#define two_RelayPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
#define two_RelayPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

#define two_RelayPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
#define two_RelayPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

/* --------------------------------------------------------------------------------------
Support functions:
* -------------------------------------------------------------------------------------- */

NDDSUSERDllExport extern two_Relay*
two_RelayPluginSupport_create_data_w_params(
    const struct DDS_TypeAllocationParams_t * alloc_params);

NDDSUSERDllExport extern two_Relay*
two_RelayPluginSupport_create_data_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern two_Relay*
two_RelayPluginSupport_create_data(void);

NDDSUSERDllExport extern RTIBool 
two_RelayPluginSupport_copy_data(
    two_Relay *out,
    const two_Relay *in);

NDDSUSERDllExport extern void 
two_RelayPluginSupport_destroy_data_w_params(
    two_Relay *sample,
    const struct DDS_TypeDeallocationParams_t * dealloc_params);

NDDSUSERDllExport extern void 
two_RelayPluginSupport_destroy_data_ex(
    two_Relay *sample,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
two_RelayPluginSupport_destroy_data(
    two_Relay *sample);

NDDSUSERDllExport extern void 
two_RelayPluginSupport_print_data(
    const two_Relay *sample,
    const char *desc,
    unsigned int indent);

/* ----------------------------------------------------------------------------
Callback functions:
* ---------------------------------------------------------------------------- */

NDDSUSERDllExport extern PRESTypePluginParticipantData 
two_RelayPlugin_on_participant_attached(
    void *registration_data, 
    const struct PRESTypePluginParticipantInfo *participant_info,
    RTIBool top_level_registration, 
    void *container_plugin_context,
    RTICdrTypeCode *typeCode);

NDDSUSERDllExport extern void 
two_RelayPlugin_on_participant_detached(
    PRESTypePluginParticipantData participant_data);

NDDSUSERDllExport extern PRESTypePluginEndpointData 
two_RelayPlugin_on_endpoint_attached(
    PRESTypePluginParticipantData participant_data,
    const struct PRESTypePluginEndpointInfo *endpoint_info,
    RTIBool top_level_registration, 
    void *container_plugin_context);

NDDSUSERDllExport extern void 
two_RelayPlugin_on_endpoint_detached(
    PRESTypePluginEndpointData endpoint_data);

NDDSUSERDllExport extern void    
two_RelayPlugin_return_sample(
    PRESTypePluginEndpointData endpoint_data,
    two_Relay *sample,
    void *handle);    

NDDSUSERDllExport extern RTIBool 
two_RelayPlugin_copy_sample(
    PRESTypePluginEndpointData endpoint_data,
    two_Relay *out,
    const two_Relay *in);

/* ----------------------------------------------------------------------------
(De)Serialize functions:
* ------------------------------------------------------------------------- */

NDDSUSERDllExport extern RTIBool 
two_RelayPlugin_serialize(
    PRESTypePluginEndpointData endpoint_data,
    const two_Relay *sample,
    struct RTICdrStream *stream, 
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
two_RelayPlugin_deserialize_sample(
    PRESTypePluginEndpointData endpoint_data,
    two_Relay *sample, 
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
two_RelayPlugin_serialize_to_cdr_buffer(
    char * buffer,
    unsigned int * length,
    const two_Relay *sample); 

NDDSUSERDllExport extern RTIBool 
two_RelayPlugin_deserialize(
    PRESTypePluginEndpointData endpoint_data,
    two_Relay **sample, 
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
two_RelayPlugin_deserialize_from_cdr_buffer(
    two_Relay *sample,
    const char * buffer,
    unsigned int length);    
NDDSUSERDllExport extern DDS_ReturnCode_t
two_RelayPlugin_data_to_string(
    const two_Relay *sample,
    char *str,
    DDS_UnsignedLong *str_size, 
    const struct DDS_PrintFormatProperty *property);    

NDDSUSERDllExport extern RTIBool
two_RelayPlugin_skip(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    RTIBool skip_encapsulation,  
    RTIBool skip_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern unsigned int 
two_RelayPlugin_get_serialized_sample_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);    

NDDSUSERDllExport extern unsigned int 
two_RelayPlugin_get_serialized_sample_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
two_RelayPlugin_get_serialized_sample_min_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int
two_RelayPlugin_get_serialized_sample_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment,
    const two_Relay * sample);

/* --------------------------------------------------------------------------------------
Key Management functions:
* -------------------------------------------------------------------------------------- */
NDDSUSERDllExport extern PRESTypePluginKeyKind 
two_RelayPlugin_get_key_kind(void);

NDDSUSERDllExport extern unsigned int 
two_RelayPlugin_get_serialized_key_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
two_RelayPlugin_get_serialized_key_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern RTIBool 
two_RelayPlugin_serialize_key(
    PRESTypePluginEndpointData endpoint_data,
    const two_Relay *sample,
    struct RTICdrStream *stream,
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
two_RelayPlugin_deserialize_key_sample(
    PRESTypePluginEndpointData endpoint_data,
    two_Relay * sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
two_RelayPlugin_deserialize_key(
    PRESTypePluginEndpointData endpoint_data,
    two_Relay ** sample,
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
two_RelayPlugin_serialized_sample_to_key(
    PRESTypePluginEndpointData endpoint_data,
    two_Relay *sample,
    struct RTICdrStream *stream, 
    RTIBool deserialize_encapsulation,  
    RTIBool deserialize_key, 
    void *endpoint_plugin_qos);

/* Plugin Functions */
NDDSUSERDllExport extern struct PRESTypePlugin*
two_RelayPlugin_new(void);

NDDSUSERDllExport extern void
two_RelayPlugin_delete(struct PRESTypePlugin *);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* meter_relayPlugin_1882284457_h */

