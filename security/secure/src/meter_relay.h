

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from meter_relay.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef meter_relay_1882284457_h
#define meter_relay_1882284457_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_c_h
#include "ndds/ndds_c.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

extern const char *two_MeterTYPENAME;

typedef struct two_Meter {

    DDS_Long   id ;
    DDS_Float   voltage ;
    DDS_Float   current ;
    DDS_Float   power ;
    DDS_Float   frequency ;
    DDS_Float   pf ;
    DDS_Char *   status ;

} two_Meter ;
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* two_Meter_get_typecode(void); /* Type code */

DDS_SEQUENCE(two_MeterSeq, two_Meter);

NDDSUSERDllExport
RTIBool two_Meter_initialize(
    two_Meter* self);

NDDSUSERDllExport
RTIBool two_Meter_initialize_ex(
    two_Meter* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool two_Meter_initialize_w_params(
    two_Meter* self,
    const struct DDS_TypeAllocationParams_t * allocParams);  

NDDSUSERDllExport
void two_Meter_finalize(
    two_Meter* self);

NDDSUSERDllExport
void two_Meter_finalize_ex(
    two_Meter* self,RTIBool deletePointers);

NDDSUSERDllExport
void two_Meter_finalize_w_params(
    two_Meter* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void two_Meter_finalize_optional_members(
    two_Meter* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool two_Meter_copy(
    two_Meter* dst,
    const two_Meter* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

extern const char *two_RelayTYPENAME;

typedef struct two_Relay {

    DDS_Long   id ;
    DDS_Char *   status ;

} two_Relay ;
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* two_Relay_get_typecode(void); /* Type code */

DDS_SEQUENCE(two_RelaySeq, two_Relay);

NDDSUSERDllExport
RTIBool two_Relay_initialize(
    two_Relay* self);

NDDSUSERDllExport
RTIBool two_Relay_initialize_ex(
    two_Relay* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool two_Relay_initialize_w_params(
    two_Relay* self,
    const struct DDS_TypeAllocationParams_t * allocParams);  

NDDSUSERDllExport
void two_Relay_finalize(
    two_Relay* self);

NDDSUSERDllExport
void two_Relay_finalize_ex(
    two_Relay* self,RTIBool deletePointers);

NDDSUSERDllExport
void two_Relay_finalize_w_params(
    two_Relay* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void two_Relay_finalize_optional_members(
    two_Relay* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool two_Relay_copy(
    two_Relay* dst,
    const two_Relay* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* meter_relay */

