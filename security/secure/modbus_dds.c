#include <modbus/modbus-rtu.h>
#include <modbus/modbus-tcp.h>
#include <modbus/modbus.h>

#define METER_G_MSEC_PER_SEC 1000
#define MODBUS_READ_REGISTERS_METER_VOLTAGE 20
#define MODBUS_READ_REGISTERS_METER_CURRENT 26
#define MODBUS_READ_REGISTERS_METER_POWER 30
#define MODBUS_READ_REGISTERS_METER_FREQUENCY 46
#define MODBUS_READ_REGISTERS_METER_PF 42

#define RELAY_G_MSEC_PER_SEC 1000
#define MODBUS_WRITE_REGISTER_RELAY_CH1 0x01

void read_meter()
{
    /* For Meter */
    uint16_t *tab_reg;
    uint16_t *tab_reg1;
    uint16_t *tab_reg2;
    uint16_t *tab_reg3;
    uint16_t *tab_reg4;
    modbus_t *ctx;
    int nb_points;
    int nb_addr, nb_addr1, nb_addr2, nb_addr3, nb_addr4;
    int rc;
    int flag = 0;

    printf("Use RTU\n");

    tab_reg = (uint16_t *) malloc(MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    memset(tab_reg, 0, MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    tab_reg1 = (uint16_t *) malloc(MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    memset(tab_reg1, 0, MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    tab_reg2 = (uint16_t *) malloc(MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    memset(tab_reg2, 0, MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    tab_reg3 = (uint16_t *) malloc(MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t)); 
    memset(tab_reg3, 0, MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    tab_reg4 = (uint16_t *) malloc(MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t)); 
    memset(tab_reg4, 0, MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));

    nb_points = 1;
    nb_addr = MODBUS_READ_REGISTERS_CURRENT;
    nb_addr1 = MODBUS_READ_REGISTERS_VOLTAGE;
    nb_addr2 = MODBUS_READ_REGISTERS_POWER;
    nb_addr3 = MODBUS_READ_REGISTERS_FREQUENCY;
    nb_addr4 = MODBUS_READ_REGISTERS_PF;

    printf("Writing two_Meter...\n");

    ctx = modbus_new_rtu("/dev/ttyUSB0", 19200, 'N', 8, 1);
    modbus_set_slave(ctx, 1);

    printf("READ REGISTERS\n\n");
    flag = 0;
    if (modbus_connect(ctx) == -1) {
        flag = 1;
        modbus_close(ctx);
        modbus_free(ctx);
    }

    rc = 0;
    /* Modify the data to be written here */
    rc = modbus_read_registers(ctx, nb_addr, nb_points, tab_reg);
    rc = modbus_read_registers(ctx, nb_addr1, nb_points, tab_reg1);
    rc = modbus_read_registers(ctx, nb_addr2, nb_points, tab_reg2);
    rc = modbus_read_registers(ctx, nb_addr3, nb_points, tab_reg3); 
    rc = modbus_read_registers(ctx, nb_addr4, nb_points, tab_reg4); 


    printf("Current is %.3lf\n", (double)*tab_reg*4/1000);
    printf("Voltage is %.1lf\n", (double)*tab_reg1/10);
    printf("Power is %.1lf\n", (double)*tab_reg2*0.4);
    printf("Frequency is %.2lf\n", (double)*tab_reg3/100);  
    printf("PF is %.3lf\n", (double)*tab_reg4/1000);

    instance->current = (double)*tab_reg*4/1000;
    instance->voltage = (double)*tab_reg1/10;
    instance->power  = (double)*tab_reg2*0.4;
    instance->frequency  = (double)*tab_reg3/100;
    instance->pf  = (double)*tab_reg4/1000;    

    if(!flag){
        modbus_close(ctx);
        modbus_free(ctx);
    }
}