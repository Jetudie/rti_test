

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Example.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef ExamplePlugin_276139112_h
#define ExamplePlugin_276139112_h

#include "Example.h"

struct RTICdrStream;

#ifndef pres_typePlugin_h
#include "pres/pres_typePlugin.h"
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

/* The type used to store keys for instances of type struct
* AnotherSimple.
*
* By default, this type is struct Meter
* itself. However, if for some reason this choice is not practical for your
* system (e.g. if sizeof(struct Meter)
* is very large), you may redefine this typedef in terms of another type of
* your choosing. HOWEVER, if you define the KeyHolder type to be something
* other than struct AnotherSimple, the
* following restriction applies: the key of struct
* Meter must consist of a
* single field of your redefined KeyHolder type and that field must be the
* first field in struct Meter.
*/
typedef  struct Meter MeterKeyHolder;

#define MeterPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
#define MeterPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
#define MeterPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

#define MeterPlugin_get_key PRESTypePluginDefaultEndpointData_getKey 
#define MeterPlugin_return_key PRESTypePluginDefaultEndpointData_returnKey

#define MeterPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
#define MeterPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

/* --------------------------------------------------------------------------------------
Support functions:
* -------------------------------------------------------------------------------------- */

NDDSUSERDllExport extern Meter*
MeterPluginSupport_create_data_w_params(
    const struct DDS_TypeAllocationParams_t * alloc_params);

NDDSUSERDllExport extern Meter*
MeterPluginSupport_create_data_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern Meter*
MeterPluginSupport_create_data(void);

NDDSUSERDllExport extern RTIBool 
MeterPluginSupport_copy_data(
    Meter *out,
    const Meter *in);

NDDSUSERDllExport extern void 
MeterPluginSupport_destroy_data_w_params(
    Meter *sample,
    const struct DDS_TypeDeallocationParams_t * dealloc_params);

NDDSUSERDllExport extern void 
MeterPluginSupport_destroy_data_ex(
    Meter *sample,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
MeterPluginSupport_destroy_data(
    Meter *sample);

NDDSUSERDllExport extern void 
MeterPluginSupport_print_data(
    const Meter *sample,
    const char *desc,
    unsigned int indent);

NDDSUSERDllExport extern Meter*
MeterPluginSupport_create_key_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern Meter*
MeterPluginSupport_create_key(void);

NDDSUSERDllExport extern void 
MeterPluginSupport_destroy_key_ex(
    MeterKeyHolder *key,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
MeterPluginSupport_destroy_key(
    MeterKeyHolder *key);

/* ----------------------------------------------------------------------------
Callback functions:
* ---------------------------------------------------------------------------- */

NDDSUSERDllExport extern PRESTypePluginParticipantData 
MeterPlugin_on_participant_attached(
    void *registration_data, 
    const struct PRESTypePluginParticipantInfo *participant_info,
    RTIBool top_level_registration, 
    void *container_plugin_context,
    RTICdrTypeCode *typeCode);

NDDSUSERDllExport extern void 
MeterPlugin_on_participant_detached(
    PRESTypePluginParticipantData participant_data);

NDDSUSERDllExport extern PRESTypePluginEndpointData 
MeterPlugin_on_endpoint_attached(
    PRESTypePluginParticipantData participant_data,
    const struct PRESTypePluginEndpointInfo *endpoint_info,
    RTIBool top_level_registration, 
    void *container_plugin_context);

NDDSUSERDllExport extern void 
MeterPlugin_on_endpoint_detached(
    PRESTypePluginEndpointData endpoint_data);

NDDSUSERDllExport extern void    
MeterPlugin_return_sample(
    PRESTypePluginEndpointData endpoint_data,
    Meter *sample,
    void *handle);    

NDDSUSERDllExport extern RTIBool 
MeterPlugin_copy_sample(
    PRESTypePluginEndpointData endpoint_data,
    Meter *out,
    const Meter *in);

/* ----------------------------------------------------------------------------
(De)Serialize functions:
* ------------------------------------------------------------------------- */

NDDSUSERDllExport extern RTIBool 
MeterPlugin_serialize(
    PRESTypePluginEndpointData endpoint_data,
    const Meter *sample,
    struct RTICdrStream *stream, 
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
MeterPlugin_deserialize_sample(
    PRESTypePluginEndpointData endpoint_data,
    Meter *sample, 
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
MeterPlugin_serialize_to_cdr_buffer(
    char * buffer,
    unsigned int * length,
    const Meter *sample); 

NDDSUSERDllExport extern RTIBool 
MeterPlugin_deserialize(
    PRESTypePluginEndpointData endpoint_data,
    Meter **sample, 
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
MeterPlugin_deserialize_from_cdr_buffer(
    Meter *sample,
    const char * buffer,
    unsigned int length);    
NDDSUSERDllExport extern DDS_ReturnCode_t
MeterPlugin_data_to_string(
    const Meter *sample,
    char *str,
    DDS_UnsignedLong *str_size, 
    const struct DDS_PrintFormatProperty *property);    

NDDSUSERDllExport extern RTIBool
MeterPlugin_skip(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    RTIBool skip_encapsulation,  
    RTIBool skip_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern unsigned int 
MeterPlugin_get_serialized_sample_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);    

NDDSUSERDllExport extern unsigned int 
MeterPlugin_get_serialized_sample_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
MeterPlugin_get_serialized_sample_min_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int
MeterPlugin_get_serialized_sample_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment,
    const Meter * sample);

/* --------------------------------------------------------------------------------------
Key Management functions:
* -------------------------------------------------------------------------------------- */
NDDSUSERDllExport extern PRESTypePluginKeyKind 
MeterPlugin_get_key_kind(void);

NDDSUSERDllExport extern unsigned int 
MeterPlugin_get_serialized_key_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
MeterPlugin_get_serialized_key_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern RTIBool 
MeterPlugin_serialize_key(
    PRESTypePluginEndpointData endpoint_data,
    const Meter *sample,
    struct RTICdrStream *stream,
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
MeterPlugin_deserialize_key_sample(
    PRESTypePluginEndpointData endpoint_data,
    Meter * sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
MeterPlugin_deserialize_key(
    PRESTypePluginEndpointData endpoint_data,
    Meter ** sample,
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
MeterPlugin_serialized_sample_to_key(
    PRESTypePluginEndpointData endpoint_data,
    Meter *sample,
    struct RTICdrStream *stream, 
    RTIBool deserialize_encapsulation,  
    RTIBool deserialize_key, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
MeterPlugin_instance_to_key(
    PRESTypePluginEndpointData endpoint_data,
    MeterKeyHolder *key, 
    const Meter *instance);

NDDSUSERDllExport extern RTIBool 
MeterPlugin_key_to_instance(
    PRESTypePluginEndpointData endpoint_data,
    Meter *instance, 
    const MeterKeyHolder *key);

NDDSUSERDllExport extern RTIBool 
MeterPlugin_instance_to_keyhash(
    PRESTypePluginEndpointData endpoint_data,
    DDS_KeyHash_t *keyhash,
    const Meter *instance);

NDDSUSERDllExport extern RTIBool 
MeterPlugin_serialized_sample_to_keyhash(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    DDS_KeyHash_t *keyhash,
    RTIBool deserialize_encapsulation,
    void *endpoint_plugin_qos); 

/* Plugin Functions */
NDDSUSERDllExport extern struct PRESTypePlugin*
MeterPlugin_new(void);

NDDSUSERDllExport extern void
MeterPlugin_delete(struct PRESTypePlugin *);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* ExamplePlugin_276139112_h */

