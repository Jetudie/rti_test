#include <stdio.h>
#include <stdlib.h>
#include "ndds/ndds_c.h"
#include "Example.h"
#include "ExampleSupport.h"

#include <stdio.h>
#ifndef _MSC_VER
#include <unistd.h>
#include <sys/time.h>
#endif
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>

#include <modbus/modbus-rtu.h>
#include <modbus/modbus-tcp.h>
#include <modbus/modbus.h>

#define G_MSEC_PER_SEC 1000
#define MODBUS_READ_REGISTERS_VOLTAGE 20
#define MODBUS_READ_REGISTERS_CURRENT 26
#define MODBUS_READ_REGISTERS_POWER 30

enum {
    TCP,
    RTU
};

/* Delete all entities */
static int publisher_shutdown(
    DDS_DomainParticipant *participant)
{
    DDS_ReturnCode_t retcode;
    int status = 0;

    if (participant != NULL) {
        retcode = DDS_DomainParticipant_delete_contained_entities(participant);
        if (retcode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_contained_entities error %d\n", retcode);
            status = -1;
        }

        retcode = DDS_DomainParticipantFactory_delete_participant(
            DDS_TheParticipantFactory, participant);
        if (retcode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_participant error %d\n", retcode);
            status = -1;
        }
    }

    /* RTI Data Distribution Service provides finalize_instance() method on
    domain participant factory for people who want to release memory used
    by the participant factory. Uncomment the following block of code for
    clean destruction of the singleton. */
    /*
    retcode = DDS_DomainParticipantFactory_finalize_instance();
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "finalize_instance error %d\n", retcode);
        status = -1;
    }
    */

    return status;
}

int publisher_main(int domainId, int sample_count)
{
    DDS_DomainParticipant *participant = NULL;
    DDS_Publisher *publisher = NULL;
    DDS_Topic *topic = NULL;
    DDS_DataWriter *writer = NULL;
    MeterDataWriter *Meter_writer = NULL;
    Meter *instance = NULL;
    DDS_ReturnCode_t retcode;
    DDS_InstanceHandle_t instance_handle = DDS_HANDLE_NIL;
    const char *type_name = NULL;
    int count = 0;  
    struct DDS_Duration_t send_period = {1,0};

    /* For Modbus */
    uint16_t *tab_reg;
    uint16_t *tab_reg1;
    uint16_t *tab_reg2;
    modbus_t *ctx;
    int nb_points;
    int nb_addr, nb_addr1, nb_addr2;
    int rc;
    int flag = 0;

    printf("Use RTU\n");

    tab_reg = (uint16_t *) malloc(MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    memset(tab_reg, 0, MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));

    tab_reg1 = (uint16_t *) malloc(MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    memset(tab_reg1, 0, MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));

    tab_reg2 = (uint16_t *) malloc(MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    memset(tab_reg2, 0, MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    

    nb_points = 1;
    nb_addr = MODBUS_READ_REGISTERS_CURRENT;
    nb_addr1 = MODBUS_READ_REGISTERS_VOLTAGE;
    nb_addr2 = MODBUS_READ_REGISTERS_POWER;




    /* To customize participant QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    participant = DDS_DomainParticipantFactory_create_participant(
        DDS_TheParticipantFactory, domainId, &DDS_PARTICIPANT_QOS_DEFAULT,
        NULL /* listener */, DDS_STATUS_MASK_NONE);
    if (participant == NULL) {
        fprintf(stderr, "create_participant error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* To customize publisher QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    publisher = DDS_DomainParticipant_create_publisher(
        participant, &DDS_PUBLISHER_QOS_DEFAULT, NULL /* listener */,
        DDS_STATUS_MASK_NONE);
    if (publisher == NULL) {
        fprintf(stderr, "create_publisher error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* Register type before creating topic */
    type_name = MeterTypeSupport_get_type_name();
    retcode = MeterTypeSupport_register_type(
        participant, type_name);
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "register_type error %d\n", retcode);
        publisher_shutdown(participant);
        return -1;
    }

    /* To customize topic QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    topic = DDS_DomainParticipant_create_topic(
        participant, "Example Meter",
        type_name, &DDS_TOPIC_QOS_DEFAULT, NULL /* listener */,
        DDS_STATUS_MASK_NONE);
    if (topic == NULL) {
        fprintf(stderr, "create_topic error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* To customize data writer QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    writer = DDS_Publisher_create_datawriter(
        publisher, topic,
        &DDS_DATAWRITER_QOS_DEFAULT, NULL /* listener */, DDS_STATUS_MASK_NONE);
    if (writer == NULL) {
        fprintf(stderr, "create_datawriter error\n");
        publisher_shutdown(participant);
        return -1;
    }
    Meter_writer = MeterDataWriter_narrow(writer);
    if (Meter_writer == NULL) {
        fprintf(stderr, "DataWriter narrow error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* Create data sample for writing */
    instance = MeterTypeSupport_create_data_ex(DDS_BOOLEAN_TRUE);
    if (instance == NULL) {
        fprintf(stderr, "MeterTypeSupport_create_data error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* For a data type that has a key, if the same instance is going to be
    written multiple times, initialize the key here
    and register the keyed instance prior to writing */
    /*
    instance_handle = MeterDataWriter_register_instance(
        Meter_writer, instance);
    */

    /* Main loop */
    for (count=0; (sample_count == 0) || (count < sample_count); ++count) {

        printf("Writing Meter, count %d\n", count);
	ctx = modbus_new_rtu("/dev/ttyUSB0", 19200, 'N', 8, 1);
	modbus_set_slave(ctx, 1);
	
	printf("READ REGISTERS\n\n");
	flag = 0;
	if (modbus_connect(ctx) == -1) {
	    //fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
	    //modbus_free(ctx);
	    //rc = -1;
	    flag = 1;
	    modbus_close(ctx);
	    modbus_free(ctx);
	}

	rc = 0;
        /* Modify the data to be written here */
        rc = modbus_read_registers(ctx, nb_addr, nb_points, tab_reg);
        rc = modbus_read_registers(ctx, nb_addr1, nb_points, tab_reg1);
        rc = modbus_read_registers(ctx, nb_addr2, nb_points, tab_reg2);
        if (rc == -1) {
            //fprintf(stderr, "%s\n", modbus_strerror(errno));
	    printf("Current is %.1lf\n", -1.0f);
            printf("Voltage is %.1lf\n", -1.0f);
	    printf("Power is %.1lf\n", -1.0f);

	    instance->id = 9527;
            instance->current =  -1.0f;
	    instance->voltage = -1.0f;
	    instance->power  = -1.0f;
        } else{
	    printf("Current is %.1lf\n", (double)*tab_reg*4/1000);
            printf("Voltage is %.1lf\n", (double)*tab_reg1/10);
	    printf("Power is %.1lf\n", (double)*tab_reg2*0.4);

	    instance->id = 9527;
            instance->current = (double)*tab_reg*4/1000;
	    instance->voltage = (double)*tab_reg1/10;
	    instance->power  = (double)*tab_reg2*0.4;
	}

	if(instance->power == -1){
	    sprintf(instance->status, "Off");
	} else {
	    sprintf(instance->status, "On");
	}

        /* Write data */
        retcode = MeterDataWriter_write(
            Meter_writer, instance, &instance_handle);
        if (retcode != DDS_RETCODE_OK) {
            fprintf(stderr, "write error %d\n", retcode);
        }

        NDDS_Utility_sleep(&send_period);
	if(!flag){
            modbus_close(ctx);
            modbus_free(ctx);
	}
    }

    /*
    retcode = MeterDataWriter_unregister_instance(
        Meter_writer, instance, &instance_handle);
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "unregister instance error %d\n", retcode);
    }
    */

    /* Delete data sample */
    retcode = MeterTypeSupport_delete_data_ex(instance, DDS_BOOLEAN_TRUE);
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "MeterTypeSupport_delete_data error %d\n", retcode);
    }

    /* Free the memory */
    free(tab_reg);
    free(tab_reg1);
    free(tab_reg2);

    /* Close the connection */
    modbus_close(ctx);
    modbus_free(ctx);
    /* Cleanup and delete delete all entities */         
    return publisher_shutdown(participant);
}

int main(int argc, char *argv[])
{
    int domainId = 0;
    int sample_count = 0; /* infinite loop */

    if (argc >= 2) {
        domainId = atoi(argv[1]);
    }
    if (argc >= 3) {
        sample_count = atoi(argv[2]);
    }
    /* Uncomment this to turn on additional logging
    NDDS_Config_Logger_set_verbosity_by_category(
        NDDS_Config_Logger_get_instance(),
        NDDS_CONFIG_LOG_CATEGORY_API, 
        NDDS_CONFIG_LOG_VERBOSITY_STATUS_ALL);
    */


    return publisher_main(domainId, sample_count);
}

