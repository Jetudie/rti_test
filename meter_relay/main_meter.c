#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "ndds/ndds_c.h"
#include "meter_relay.h"
#include "meter_relaySupport.h"

#include <modbus/modbus-rtu.h>
#include <modbus/modbus-tcp.h>
#include <modbus/modbus.h>

#define G_MSEC_PER_SEC 1000
#define MODBUS_READ_REGISTERS_VOLTAGE 20
#define MODBUS_READ_REGISTERS_CURRENT 26
#define MODBUS_READ_REGISTERS_POWER 30
#define MODBUS_READ_REGISTERS_FREQUENCY 46
#define MODBUS_READ_REGISTERS_PF 42

/* Delete all entities */
static int publisher_shutdown(
    DDS_DomainParticipant *participant)
{
    DDS_ReturnCode_t retcode;
    int status = 0;

    if (participant != NULL) {
        retcode = DDS_DomainParticipant_delete_contained_entities(participant);
        if (retcode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_contained_entities error %d\n", retcode);
            status = -1;
        }

        retcode = DDS_DomainParticipantFactory_delete_participant(
            DDS_TheParticipantFactory, participant);
        if (retcode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_participant error %d\n", retcode);
            status = -1;
        }
    }
    return status;
}

/* For meter publisher */
void *publisher_meter(void* arg)
{ 
    int *input = (int*) arg;
    int domainId = input[0];
    int sample_count = input[1];
    DDS_DomainParticipant *participant = NULL;
    DDS_Publisher *publisher = NULL;
    DDS_Topic *topic = NULL;
    DDS_DataWriter *writer = NULL;
    two_MeterDataWriter *two_Meter_writer = NULL;
    two_Meter *instance = NULL;
    DDS_ReturnCode_t retcode;
    DDS_InstanceHandle_t instance_handle = DDS_HANDLE_NIL;
    const char *type_name = NULL;
    int count = 0;  
    struct DDS_Duration_t send_period = {0.1,0};
    /* For Modbus */
    uint16_t *tab_reg;
    uint16_t *tab_reg1;
    uint16_t *tab_reg2;
    uint16_t *tab_reg3;
    uint16_t *tab_reg4;
    modbus_t *ctx;
    int nb_points;
    int nb_addr, nb_addr1, nb_addr2, nb_addr3, nb_addr4;
    int rc;
    int flag = 0;

    printf("Use RTU\n");

    tab_reg = (uint16_t *) malloc(MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    memset(tab_reg, 0, MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    tab_reg1 = (uint16_t *) malloc(MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    memset(tab_reg1, 0, MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    tab_reg2 = (uint16_t *) malloc(MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    memset(tab_reg2, 0, MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    tab_reg3 = (uint16_t *) malloc(MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t)); 
    memset(tab_reg3, 0, MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));
    tab_reg4 = (uint16_t *) malloc(MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t)); 
    memset(tab_reg4, 0, MODBUS_MAX_READ_REGISTERS * sizeof(uint16_t));

    nb_points = 1;
    nb_addr = MODBUS_READ_REGISTERS_CURRENT;
    nb_addr1 = MODBUS_READ_REGISTERS_VOLTAGE;
    nb_addr2 = MODBUS_READ_REGISTERS_POWER;
    nb_addr3 = MODBUS_READ_REGISTERS_FREQUENCY;
    nb_addr4 = MODBUS_READ_REGISTERS_PF;

    /* To customize participant QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    participant = DDS_DomainParticipantFactory_create_participant(
        DDS_TheParticipantFactory, domainId, &DDS_PARTICIPANT_QOS_DEFAULT,
        NULL /* listener */, DDS_STATUS_MASK_NONE);
    if (participant == NULL) {
        fprintf(stderr, "create_participant error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* To customize publisher QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    publisher = DDS_DomainParticipant_create_publisher(
        participant, &DDS_PUBLISHER_QOS_DEFAULT, NULL /* listener */,
        DDS_STATUS_MASK_NONE);
    if (publisher == NULL) {
        fprintf(stderr, "create_publisher error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* Register type before creating topic */
    type_name = two_MeterTypeSupport_get_type_name();
    retcode = two_MeterTypeSupport_register_type(
        participant, type_name);
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "register_type error %d\n", retcode);
        publisher_shutdown(participant);
        return -1;
    }

    /* To customize topic QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    topic = DDS_DomainParticipant_create_topic(
        participant, "Example two_Meter",
        type_name, &DDS_TOPIC_QOS_DEFAULT, NULL /* listener */,
        DDS_STATUS_MASK_NONE);
    if (topic == NULL) {
        fprintf(stderr, "create_topic error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* To customize data writer QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    writer = DDS_Publisher_create_datawriter(
        publisher, topic,
        &DDS_DATAWRITER_QOS_DEFAULT, NULL /* listener */, DDS_STATUS_MASK_NONE);
    if (writer == NULL) {
        fprintf(stderr, "create_datawriter error\n");
        publisher_shutdown(participant);
        return -1;
    }
    two_Meter_writer = two_MeterDataWriter_narrow(writer);
    if (two_Meter_writer == NULL) {
        fprintf(stderr, "DataWriter narrow error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* Create data sample for writing */
    instance = two_MeterTypeSupport_create_data_ex(DDS_BOOLEAN_TRUE);
    if (instance == NULL) {
        fprintf(stderr, "two_MeterTypeSupport_create_data error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* For a data type that has a key, if the same instance is going to be
    written multiple times, initialize the key here
    and register the keyed instance prior to writing */
    /*
    instance_handle = two_MeterDataWriter_register_instance(
        two_Meter_writer, instance);
    */

    /* Main loop */
    for (count=0; (sample_count == 0) || (count < sample_count); ++count) {

        printf("Writing two_Meter, count %d\n", count);

        ctx = modbus_new_rtu("/dev/ttyUSB0", 19200, 'N', 8, 1);
        modbus_set_slave(ctx, 1);

        printf("READ REGISTERS\n\n");
        flag = 0;
        if (modbus_connect(ctx) == -1) {
            flag = 1;
            modbus_close(ctx);
            modbus_free(ctx);
        }

        rc = 0;
        /* Modify the data to be written here */
        rc = modbus_read_registers(ctx, nb_addr, nb_points, tab_reg);
        rc = modbus_read_registers(ctx, nb_addr1, nb_points, tab_reg1);
        rc = modbus_read_registers(ctx, nb_addr2, nb_points, tab_reg2);
        rc = modbus_read_registers(ctx, nb_addr3, nb_points, tab_reg3); 
        rc = modbus_read_registers(ctx, nb_addr4, nb_points, tab_reg4); 

        if (rc == -1) {
            //fprintf(stderr, "%s\n", modbus_strerror(errno));
            printf("Current is %.1lf\n", -1.0f);
            printf("Voltage is %.1lf\n", -1.0f);
            printf("Power is %.1lf\n", -1.0f);
            printf("Frequency is %.1lf\n", -1.0f);
            printf("PF is %.1lf\n", -1.0f);

            instance->id = 9527;
            instance->current =  -1.0f;
            instance->voltage = -1.0f;
            instance->power  = -1.0f;
            instance->frequency = -1.0f;
            instance->pf = -1.0f;

        } else{
            printf("Current is %.3lf\n", (double)*tab_reg*4/1000);
            printf("Voltage is %.1lf\n", (double)*tab_reg1/10);
            printf("Power is %.1lf\n", (double)*tab_reg2*0.4);
            printf("Frequency is %.2lf\n", (double)*tab_reg3/100);  
            printf("PF is %.3lf\n", (double)*tab_reg4/1000);

            instance->id = count;
            instance->current = (double)*tab_reg*4/1000;
            instance->voltage = (double)*tab_reg1/10;
            instance->power  = (double)*tab_reg2*0.4;
            instance->frequency  = (double)*tab_reg3/100;
            instance->pf  = (double)*tab_reg4/1000;    
        }

        if(instance->power == -1){
            sprintf(instance->status, "Off");
        } else {
            sprintf(instance->status, "On");
        }
        printf("status is %s\n", instance->status);

        if(!flag){
            modbus_close(ctx);
            modbus_free(ctx);
        }
        /* Write data */
        retcode = two_MeterDataWriter_write(
            two_Meter_writer, instance, &instance_handle);
        if (retcode != DDS_RETCODE_OK) {
            fprintf(stderr, "write error %d\n", retcode);
        }

        NDDS_Utility_sleep(&send_period);
    }

    /* Free the memory */
    free(tab_reg);
    free(tab_reg1);
    free(tab_reg2);
    free(tab_reg3); 
    free(tab_reg4);
 
    /* Close the connection */
    modbus_close(ctx);
    modbus_free(ctx);

    /*
    retcode = two_MeterDataWriter_unregister_instance(
        two_Meter_writer, instance, &instance_handle);
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "unregister instance error %d\n", retcode);
    }
    */

    /* Delete data sample */
    retcode = two_MeterTypeSupport_delete_data_ex(instance, DDS_BOOLEAN_TRUE);
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "two_MeterTypeSupport_delete_data error %d\n", retcode);
    }

    /* Cleanup and delete delete all entities */         
    return publisher_shutdown(participant);
}

/* For Relay Subscriber */
void two_RelayListener_on_requested_deadline_missed(
    void* listener_data,
    DDS_DataReader* reader,
    const struct DDS_RequestedDeadlineMissedStatus *status)
{
}

void two_RelayListener_on_requested_incompatible_qos(
    void* listener_data,
    DDS_DataReader* reader,
    const struct DDS_RequestedIncompatibleQosStatus *status)
{
}

void two_RelayListener_on_sample_rejected(
    void* listener_data,
    DDS_DataReader* reader,
    const struct DDS_SampleRejectedStatus *status)
{
}

void two_RelayListener_on_liveliness_changed(
    void* listener_data,
    DDS_DataReader* reader,
    const struct DDS_LivelinessChangedStatus *status)
{
}

void two_RelayListener_on_sample_lost(
    void* listener_data,
    DDS_DataReader* reader,
    const struct DDS_SampleLostStatus *status)
{
}

void two_RelayListener_on_subscription_matched(
    void* listener_data,
    DDS_DataReader* reader,
    const struct DDS_SubscriptionMatchedStatus *status)
{
}

void two_RelayListener_on_data_available(
    void* listener_data,
    DDS_DataReader* reader)
{
    two_RelayDataReader *two_Relay_reader = NULL;
    struct two_RelaySeq data_seq = DDS_SEQUENCE_INITIALIZER;
    struct DDS_SampleInfoSeq info_seq = DDS_SEQUENCE_INITIALIZER;
    DDS_ReturnCode_t retcode;
    int i;

    two_Relay_reader = two_RelayDataReader_narrow(reader);
    if (two_Relay_reader == NULL) {
        fprintf(stderr, "DataReader narrow error\n");
        return;
    }

    retcode = two_RelayDataReader_take(
        two_Relay_reader,
        &data_seq, &info_seq, DDS_LENGTH_UNLIMITED,
        DDS_ANY_SAMPLE_STATE, DDS_ANY_VIEW_STATE, DDS_ANY_INSTANCE_STATE);
    if (retcode == DDS_RETCODE_NO_DATA) {
        return;
    } else if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "take error %d\n", retcode);
        return;
    }

    for (i = 0; i < two_RelaySeq_get_length(&data_seq); ++i) {
        if (DDS_SampleInfoSeq_get_reference(&info_seq, i)->valid_data) {
            printf("Received data\n");
            //two_RelayTypeSupport_print_data(
            //    two_RelaySeq_get_reference(&data_seq, i));

            printf("\n");
            printf("status: %s\n", two_RelaySeq_get_reference(&data_seq, i)->status);
            printf("\n");
        }
    }

    retcode = two_RelayDataReader_return_loan(
        two_Relay_reader,
        &data_seq, &info_seq);
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "return loan error %d\n", retcode);
    }
}

/* Delete all entities */
static int subscriber_shutdown(
    DDS_DomainParticipant *participant)
{
    DDS_ReturnCode_t retcode;
    int status = 0;

    if (participant != NULL) {
        retcode = DDS_DomainParticipant_delete_contained_entities(participant);
        if (retcode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_contained_entities error %d\n", retcode);
            status = -1;
        }

        retcode = DDS_DomainParticipantFactory_delete_participant(
            DDS_TheParticipantFactory, participant);
        if (retcode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_participant error %d\n", retcode);
            status = -1;
        }
    }

    return status;
}

void *subscriber_relay(void* arg)
{ 
    int *input = (int*) arg;
    int domainId = input[0];
    int sample_count = input[1];
    DDS_DomainParticipant *participant = NULL;
    DDS_Subscriber *subscriber = NULL;
    DDS_Topic *topic = NULL;
    struct DDS_DataReaderListener reader_listener =
    DDS_DataReaderListener_INITIALIZER;
    DDS_DataReader *reader = NULL;
    DDS_ReturnCode_t retcode;
    const char *type_name = NULL;
    int count = 0;
    struct DDS_Duration_t poll_period = {0.1,0};

    /* To customize participant QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    participant = DDS_DomainParticipantFactory_create_participant(
        DDS_TheParticipantFactory, domainId, &DDS_PARTICIPANT_QOS_DEFAULT,
        NULL /* listener */, DDS_STATUS_MASK_NONE);
    if (participant == NULL) {
        fprintf(stderr, "create_participant error\n");
        subscriber_shutdown(participant);
        return -1;
    }

    /* To customize subscriber QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    subscriber = DDS_DomainParticipant_create_subscriber(
        participant, &DDS_SUBSCRIBER_QOS_DEFAULT, NULL /* listener */,
        DDS_STATUS_MASK_NONE);
    if (subscriber == NULL) {
        fprintf(stderr, "create_subscriber error\n");
        subscriber_shutdown(participant);
        return -1;
    }

    /* Register the type before creating the topic */
    type_name = two_RelayTypeSupport_get_type_name();
    retcode = two_RelayTypeSupport_register_type(participant, type_name);
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "register_type error %d\n", retcode);
        subscriber_shutdown(participant);
        return -1;
    }

    /* To customize topic QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    topic = DDS_DomainParticipant_create_topic(
        participant, "Example two_Relay",
        type_name, &DDS_TOPIC_QOS_DEFAULT, NULL /* listener */,
        DDS_STATUS_MASK_NONE);
    if (topic == NULL) {
        fprintf(stderr, "create_topic error\n");
        subscriber_shutdown(participant);
        return -1;
    }

    /* Set up a data reader listener */
    reader_listener.on_requested_deadline_missed  =
    two_RelayListener_on_requested_deadline_missed;
    reader_listener.on_requested_incompatible_qos =
    two_RelayListener_on_requested_incompatible_qos;
    reader_listener.on_sample_rejected =
    two_RelayListener_on_sample_rejected;
    reader_listener.on_liveliness_changed =
    two_RelayListener_on_liveliness_changed;
    reader_listener.on_sample_lost =
    two_RelayListener_on_sample_lost;
    reader_listener.on_subscription_matched =
    two_RelayListener_on_subscription_matched;
    reader_listener.on_data_available =
    two_RelayListener_on_data_available;

    /* To customize data reader QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    reader = DDS_Subscriber_create_datareader(
        subscriber, DDS_Topic_as_topicdescription(topic),
        &DDS_DATAREADER_QOS_DEFAULT, &reader_listener, DDS_STATUS_MASK_ALL);
    if (reader == NULL) {
        fprintf(stderr, "create_datareader error\n");
        subscriber_shutdown(participant);
        return -1;
    }

    /* Main loop */
    for (count=0; (sample_count == 0) || (count < sample_count); ++count) {
        //printf("two_Relay subscriber sleeping for %d sec...\n",
        //poll_period.sec);

        NDDS_Utility_sleep(&poll_period);
    }

    /* Cleanup and delete all entities */ 
    return subscriber_shutdown(participant);
}

int main(int argc, char *argv[])
{
    int domainId = 0;
    int sample_count = 0;
    int input1[3] = {domainId, 0, };
    int input2[3] = {domainId, 0, 1};
    if (argc >= 2) {
        domainId = atoi(argv[1]);
    }
    if (argc >= 3) {
        sample_count = atoi(argv[2]);
    }
    pthread_t t1, t2;
    
    pthread_create(&t1, NULL, publisher_meter, (void*)input1); //publish meter
    pthread_create(&t2, NULL, subscriber_relay, (void*)input2); //subscribe relay

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);

    return 0;
}

