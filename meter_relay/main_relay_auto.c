#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "ndds/ndds_c.h"
#include "meter_relay.h"
#include "meter_relaySupport.h"

#include <modbus/modbus-rtu.h>
#include <modbus/modbus-tcp.h>
#include <modbus/modbus.h>

#define G_MSEC_PER_SEC 1000
#define MODBUS_WRITE_REGISTER_CH1 0x01

u_int32_t on_off = 1;

/* Delete all entities */
static int publisher_shutdown(
    DDS_DomainParticipant *participant)
{
    DDS_ReturnCode_t retcode;
    int status = 0;

    if (participant != NULL) {
        retcode = DDS_DomainParticipant_delete_contained_entities(participant);
        if (retcode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_contained_entities error %d\n", retcode);
            status = -1;
        }

        retcode = DDS_DomainParticipantFactory_delete_participant(
            DDS_TheParticipantFactory, participant);
        if (retcode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_participant error %d\n", retcode);
            status = -1;
        }
    }
    return status;
}

/* For relay publisher */
void *publisher_relay(void* arg)
{ 
    int *input = (int*) arg;
    int domainId = input[0];
    int sample_count = input[1];
    DDS_DomainParticipant *participant = NULL;
    DDS_Publisher *publisher = NULL;
    DDS_Topic *topic = NULL;
    DDS_DataWriter *writer = NULL;
    two_RelayDataWriter *two_Relay_writer = NULL;
    two_Relay *instance = NULL;
    DDS_ReturnCode_t retcode;
    DDS_InstanceHandle_t instance_handle = DDS_HANDLE_NIL;
    const char *type_name = NULL;
    int count = 0;  
    struct DDS_Duration_t send_period = {0.1,0};

    /* modbus initialize */
    int nb_addr;
    int rc;
    int flag = 0;
    modbus_t *ctx;
    nb_addr = MODBUS_WRITE_REGISTER_CH1;

    /* To customize participant QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    participant = DDS_DomainParticipantFactory_create_participant(
        DDS_TheParticipantFactory, domainId, &DDS_PARTICIPANT_QOS_DEFAULT,
        NULL /* listener */, DDS_STATUS_MASK_NONE);
    if (participant == NULL) {
        fprintf(stderr, "create_participant error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* To customize publisher QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    publisher = DDS_DomainParticipant_create_publisher(
        participant, &DDS_PUBLISHER_QOS_DEFAULT, NULL /* listener */,
        DDS_STATUS_MASK_NONE);
    if (publisher == NULL) {
        fprintf(stderr, "create_publisher error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* Register type before creating topic */
    type_name = two_RelayTypeSupport_get_type_name();
    retcode = two_RelayTypeSupport_register_type(
        participant, type_name);
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "register_type error %d\n", retcode);
        publisher_shutdown(participant);
        return -1;
    }

    /* To customize topic QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    topic = DDS_DomainParticipant_create_topic(
        participant, "Example two_Relay",
        type_name, &DDS_TOPIC_QOS_DEFAULT, NULL /* listener */,
        DDS_STATUS_MASK_NONE);
    if (topic == NULL) {
        fprintf(stderr, "create_topic error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* To customize data writer QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    writer = DDS_Publisher_create_datawriter(
        publisher, topic,
        &DDS_DATAWRITER_QOS_DEFAULT, NULL /* listener */, DDS_STATUS_MASK_NONE);
    if (writer == NULL) {
        fprintf(stderr, "create_datawriter error\n");
        publisher_shutdown(participant);
        return -1;
    }
    two_Relay_writer = two_RelayDataWriter_narrow(writer);
    if (two_Relay_writer == NULL) {
        fprintf(stderr, "DataWriter narrow error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* Create data sample for writing */
    instance = two_RelayTypeSupport_create_data_ex(DDS_BOOLEAN_TRUE);
    if (instance == NULL) {
        fprintf(stderr, "two_RelayTypeSupport_create_data error\n");
        publisher_shutdown(participant);
        return -1;
    }

    /* For a data type that has a key, if the same instance is going to be
    written multiple times, initialize the key here
    and register the keyed instance prior to writing */
    /*
    instance_handle = two_RelayDataWriter_register_instance(
        two_Relay_writer, instance);
    */

    /* Main loop */
    for (count=0; (sample_count == 0) || (count < sample_count); ++count) {

        printf("Writing two_Relay, count %d\n", count);


        /* collecting modbus data */
        ctx = modbus_new_rtu("/dev/ttyUSB0", 9600, 'N', 8, 1);
        modbus_set_slave(ctx, 2);
        modbus_set_debug(ctx, ON);
        flag = 0;
        if (modbus_connect(ctx) == -1) {
            flag = 1;
            modbus_close(ctx);
            modbus_free(ctx);
	    }

        /* Modify the data to be written here */
        if(on_off == 0){
            modbus_write_register(ctx, nb_addr, 513);
			sprintf(instance->status, "OFF");	
        } else{
            modbus_write_register(ctx, nb_addr, 257);	
			sprintf(instance->status, "ON");	
        }

        if(!flag){
            modbus_close(ctx);
            modbus_free(ctx);
        }

        /* Write data */
        retcode = two_RelayDataWriter_write(
            two_Relay_writer, instance, &instance_handle);
        if (retcode != DDS_RETCODE_OK) {
            fprintf(stderr, "write error %d\n", retcode);
        }
        printf("status is %s\n", instance->status);

        NDDS_Utility_sleep(&send_period);
    }

    /* Close the connection */
    modbus_close(ctx);
    modbus_free(ctx);
    /*
    retcode = two_RelayDataWriter_unregister_instance(
        two_Relay_writer, instance, &instance_handle);
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "unregister instance error %d\n", retcode);
    }
    */

    /* Delete data sample */
    retcode = two_RelayTypeSupport_delete_data_ex(instance, DDS_BOOLEAN_TRUE);
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "two_RelayTypeSupport_delete_data error %d\n", retcode);
    }

    /* Cleanup and delete delete all entities */         
    return publisher_shutdown(participant);
}


/* For Meter Subscriber */
void two_MeterListener_on_requested_deadline_missed(
    void* listener_data,
    DDS_DataReader* reader,
    const struct DDS_RequestedDeadlineMissedStatus *status)
{
}

void two_MeterListener_on_requested_incompatible_qos(
    void* listener_data,
    DDS_DataReader* reader,
    const struct DDS_RequestedIncompatibleQosStatus *status)
{
}

void two_MeterListener_on_sample_rejected(
    void* listener_data,
    DDS_DataReader* reader,
    const struct DDS_SampleRejectedStatus *status)
{
}

void two_MeterListener_on_liveliness_changed(
    void* listener_data,
    DDS_DataReader* reader,
    const struct DDS_LivelinessChangedStatus *status)
{
}

void two_MeterListener_on_sample_lost(
    void* listener_data,
    DDS_DataReader* reader,
    const struct DDS_SampleLostStatus *status)
{
}

void two_MeterListener_on_subscription_matched(
    void* listener_data,
    DDS_DataReader* reader,
    const struct DDS_SubscriptionMatchedStatus *status)
{
}

void two_MeterListener_on_data_available(
    void* listener_data,
    DDS_DataReader* reader)
{
    two_MeterDataReader *two_Meter_reader = NULL;
    struct two_MeterSeq data_seq = DDS_SEQUENCE_INITIALIZER;
    struct DDS_SampleInfoSeq info_seq = DDS_SEQUENCE_INITIALIZER;
    DDS_ReturnCode_t retcode;
    int i;

    two_Meter_reader = two_MeterDataReader_narrow(reader);
    if (two_Meter_reader == NULL) {
        fprintf(stderr, "DataReader narrow error\n");
        return;
    }

    retcode = two_MeterDataReader_take(
        two_Meter_reader,
        &data_seq, &info_seq, DDS_LENGTH_UNLIMITED,
        DDS_ANY_SAMPLE_STATE, DDS_ANY_VIEW_STATE, DDS_ANY_INSTANCE_STATE);
    if (retcode == DDS_RETCODE_NO_DATA) {
        return;
    } else if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "take error %d\n", retcode);
        return;
    }

    for (i = 0; i < two_MeterSeq_get_length(&data_seq); ++i) {
        if (DDS_SampleInfoSeq_get_reference(&info_seq, i)->valid_data) {
            printf("Received data\n");
            //two_MeterTypeSupport_print_data(
            //    two_MeterSeq_get_reference(&data_seq, i));

            printf("\n");
            if(two_MeterSeq_get_reference(&data_seq, i)->id >= 12)
                on_off = 0;
            else	
                on_off = 1;
            printf("Id: %d\n", two_MeterSeq_get_reference(&data_seq, i)->id);
            printf("Current: %.3lf\n", two_MeterSeq_get_reference(&data_seq, i)->current);
            printf("Voltage: %.1lf\n", two_MeterSeq_get_reference(&data_seq, i)->voltage);
            printf("Power: %.1lf\n", two_MeterSeq_get_reference(&data_seq, i)->power);
            printf("Frequency: %.2lf\n", two_MeterSeq_get_reference(&data_seq, i)->frequency);
            printf("PF: %.3lf\n", two_MeterSeq_get_reference(&data_seq, i)->pf);
            printf("status: %s\n", two_MeterSeq_get_reference(&data_seq, i)->status);
            printf("\n");
        }
    }

    retcode = two_MeterDataReader_return_loan(
        two_Meter_reader,
        &data_seq, &info_seq);
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "return loan error %d\n", retcode);
    }
}

/* Delete all entities */
static int subscriber_shutdown(
    DDS_DomainParticipant *participant)
{
    DDS_ReturnCode_t retcode;
    int status = 0;

    if (participant != NULL) {
        retcode = DDS_DomainParticipant_delete_contained_entities(participant);
        if (retcode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_contained_entities error %d\n", retcode);
            status = -1;
        }

        retcode = DDS_DomainParticipantFactory_delete_participant(
            DDS_TheParticipantFactory, participant);
        if (retcode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_participant error %d\n", retcode);
            status = -1;
        }
    }

    return status;
}

void *subscriber_meter(void* arg)
{ 
    int *input = (int*) arg;
    int domainId = input[0];
    int sample_count = input[1];
    DDS_DomainParticipant *participant = NULL;
    DDS_Subscriber *subscriber = NULL;
    DDS_Topic *topic = NULL;
    struct DDS_DataReaderListener reader_listener =
    DDS_DataReaderListener_INITIALIZER;
    DDS_DataReader *reader = NULL;
    DDS_ReturnCode_t retcode;
    const char *type_name = NULL;
    int count = 0;
    struct DDS_Duration_t poll_period = {0.1,0};

    /* To customize participant QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    participant = DDS_DomainParticipantFactory_create_participant(
        DDS_TheParticipantFactory, domainId, &DDS_PARTICIPANT_QOS_DEFAULT,
        NULL /* listener */, DDS_STATUS_MASK_NONE);
    if (participant == NULL) {
        fprintf(stderr, "create_participant error\n");
        subscriber_shutdown(participant);
        return -1;
    }

    /* To customize subscriber QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    subscriber = DDS_DomainParticipant_create_subscriber(
        participant, &DDS_SUBSCRIBER_QOS_DEFAULT, NULL /* listener */,
        DDS_STATUS_MASK_NONE);
    if (subscriber == NULL) {
        fprintf(stderr, "create_subscriber error\n");
        subscriber_shutdown(participant);
        return -1;
    }

    /* Register the type before creating the topic */
    type_name = two_MeterTypeSupport_get_type_name();
    retcode = two_MeterTypeSupport_register_type(participant, type_name);
    if (retcode != DDS_RETCODE_OK) {
        fprintf(stderr, "register_type error %d\n", retcode);
        subscriber_shutdown(participant);
        return -1;
    }

    /* To customize topic QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    topic = DDS_DomainParticipant_create_topic(
        participant, "Example two_Meter",
        type_name, &DDS_TOPIC_QOS_DEFAULT, NULL /* listener */,
        DDS_STATUS_MASK_NONE);
    if (topic == NULL) {
        fprintf(stderr, "create_topic error\n");
        subscriber_shutdown(participant);
        return -1;
    }

    /* Set up a data reader listener */
    reader_listener.on_requested_deadline_missed  =
    two_MeterListener_on_requested_deadline_missed;
    reader_listener.on_requested_incompatible_qos =
    two_MeterListener_on_requested_incompatible_qos;
    reader_listener.on_sample_rejected =
    two_MeterListener_on_sample_rejected;
    reader_listener.on_liveliness_changed =
    two_MeterListener_on_liveliness_changed;
    reader_listener.on_sample_lost =
    two_MeterListener_on_sample_lost;
    reader_listener.on_subscription_matched =
    two_MeterListener_on_subscription_matched;
    reader_listener.on_data_available =
    two_MeterListener_on_data_available;

    /* To customize data reader QoS, use 
    the configuration file USER_QOS_PROFILES.xml */
    reader = DDS_Subscriber_create_datareader(
        subscriber, DDS_Topic_as_topicdescription(topic),
        &DDS_DATAREADER_QOS_DEFAULT, &reader_listener, DDS_STATUS_MASK_ALL);
    if (reader == NULL) {
        fprintf(stderr, "create_datareader error\n");
        subscriber_shutdown(participant);
        return -1;
    }

    /* Main loop */
    for (count=0; (sample_count == 0) || (count < sample_count); ++count) {
        //printf("two_Meter subscriber sleeping for %d sec...\n",
        //poll_period.sec);

        NDDS_Utility_sleep(&poll_period);
    }

    /* Cleanup and delete all entities */ 
    return subscriber_shutdown(participant);
}
int main(int argc, char *argv[])
{
    int domainId = 0;
    int sample_count = 0;
    int input1[3] = {domainId, 0, 1};
    int input2[3] = {domainId, 0, 1};
    if (argc >= 2) {
        domainId = atoi(argv[1]);
    }
    if (argc >= 3) {
        sample_count = atoi(argv[2]);
    }
    pthread_t t1, t2;
    
    pthread_create(&t1, NULL, publisher_relay, (void*)input1); //publish relay
    pthread_create(&t2, NULL, subscriber_meter, (void*)input2); //subscribe meter

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);

    return 0;
}

